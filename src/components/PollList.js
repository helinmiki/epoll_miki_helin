import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';

const Poll = props => (
    <tr>
        <td>{props.poll.id}</td>
        <td>{props.poll.title}</td>

        <td>
            <Link className="btn btn-outline-success my-0 my-sm-0" to={"/details/" + props.poll.id}>View</Link>
        </td>
    </tr>
)

export default class PollList extends Component
{
    constructor(props)
    {
        super(props);

        this.state = { polls: [] };
    }

    componentDidMount() {
        axios.get('http://localhost:5000/polls')
             .then(response => {
                this.setState({ polls: response.data })
             })
             .catch((error) => {
             console.log(error);
             })
    }

    pollList() {
        return this.state.polls.map(currentpoll => {
            return <Poll poll={currentpoll}/>
        })
    }

    render() {
        return (
            <div>
                <h3>Current polls</h3>
                <table className="table">
                    <thead className="thead-light">
                        <tr>
                            <th>Id</th>
                            <th>Title</th>
                        </tr>
                    </thead>
                    <tbody>
                        { this.pollList() }
                    </tbody>
                </table>
            </div>           
        )
    }
}