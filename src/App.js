//import React from 'react';
import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.css';
import './App.css';

import Navbar from "./components/Navbar.js";
import PollList from "./components/PollList.js";
//import PollDetails from "./Components/PollDetails";
//import SearchPoll from "./Components/SearchPoll";

function App() {
    
  return (
    <Router>
        <div className="container">

           <Navbar />
           <br />
           <Route path="/" exact component={PollList} />

        </div>
    </Router>
  );
}

export default App;
